#ifndef __common_constraints__
#define __common_constraints__

#include "Eigen/Dense"
#include <ros/console.h>
#include <eigen_matrix_utils/eiquadprog.hpp>
#include <rosdyn_core/primitives.h>
#include <rosdyn_core/frame_distance.h>
#include <task_math/task_math.h>

namespace taskQP
{

namespace math
{

/****************************
 *
 *  UpperAccelerationLimits
 *
 ****************************/

class UpperAccelerationLimits : public MotionConstraint
{
public:
  UpperAccelerationLimits();
  UpperAccelerationLimits(const virtualModelPtr& p_mpc_model);
  UpperAccelerationLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
};
typedef std::shared_ptr<UpperAccelerationLimits> UpperAccelerationLimitsPtr;

/****************************
 *
 *  LowerAccelerationLimits
 *
 ****************************/

class LowerAccelerationLimits : public MotionConstraint
{
public:
  LowerAccelerationLimits();
  LowerAccelerationLimits(const virtualModelPtr& p_mpc_model);
  LowerAccelerationLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
};
typedef std::shared_ptr<LowerAccelerationLimits> LowerAccelerationLimitsPtr;

/***********************
 *
 *  UpperVelocityLimits
 *
 ***********************/

class UpperVelocityLimits : public MotionConstraint
{
public:
  UpperVelocityLimits();
  UpperVelocityLimits(const virtualModelPtr& p_mpc_model);
  UpperVelocityLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<UpperVelocityLimits> UpperVelocityLimitsPtr;

/***********************
 *
 *  LowerVelocityLimits
 *
 ***********************/

class LowerVelocityLimits : public MotionConstraint
{
public:
  LowerVelocityLimits();
  LowerVelocityLimits(const virtualModelPtr& p_mpc_model);
  LowerVelocityLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<LowerVelocityLimits> LowerVelocityLimitsPtr;

/***********************
 *
 *  UpperPositionLimits
 *
 ***********************/

class UpperPositionLimits : public MotionConstraint
{
public:
  UpperPositionLimits();
  UpperPositionLimits(const virtualModelPtr& p_mpc_model);
  UpperPositionLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<UpperPositionLimits> UpperPositionLimitsPtr;

/***********************
 *
 *  LowerPositionLimits
 *
 ***********************/

class LowerPositionLimits : public MotionConstraint
{
public:
  LowerPositionLimits();
  LowerPositionLimits(const virtualModelPtr& p_mpc_model);
  LowerPositionLimits(const virtualModelPtr& p_mpc_model,std::vector<double>& limits);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& limits);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<LowerPositionLimits> LowerPositionLimitsPtr;

/*****************************
 *
 *  UpperInvarianceConstraint
 *
 *****************************/

class UpperInvarianceConstraint : public MotionConstraint
{
public:
  UpperInvarianceConstraint();
  UpperInvarianceConstraint(const virtualModelPtr& p_mpc_model);
  UpperInvarianceConstraint(const virtualModelPtr& p_mpc_model, std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmin);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmin);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<UpperInvarianceConstraint> UpperInvarianceConstraintPtr;

/*****************************
 *
 *  LowerInvarianceConstraint
 *
 *****************************/

class LowerInvarianceConstraint : public MotionConstraint
{
public:
  LowerInvarianceConstraint();
  LowerInvarianceConstraint(const virtualModelPtr& p_mpc_model);
  LowerInvarianceConstraint(const virtualModelPtr& p_mpc_model, std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmax);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(std::vector<double>& qmin, std::vector<double>& dqmin, std::vector<double>& ddqmax);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<LowerInvarianceConstraint> LowerInvarianceConstraintPtr;

/***********************
 *
 *  UpperPositionVariation
 *
 ***********************/

class UpperPositionVariation : public MotionConstraint
{
protected:
  Eigen::VectorXd m_delta_q_max;
  Eigen::VectorXd m_q_max;
  Eigen::VectorXd m_q_sum;
  Eigen::VectorXd m_delta_q_target;
  Eigen::VectorXd m_q_target0;
public:
  UpperPositionVariation();
  UpperPositionVariation(const virtualModelPtr& p_mpc_model);
  UpperPositionVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max);
  void init(const virtualModelPtr& p_mpc_model);
  void setMechanicalJointLimits(const Eigen::VectorXd& q_max);
  void setLimits(std::vector<double>& delta_q_max);
  void setLimits(const Eigen::VectorXd& delta_q_max);
  void setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<UpperPositionVariation> UpperPositionVariationPtr;

/***********************
 *
 *  LowerPositionVariation
 *
 ***********************/

class LowerPositionVariation : public MotionConstraint
{
protected:
  Eigen::VectorXd m_delta_q_max;
  Eigen::VectorXd m_q_min;
  Eigen::VectorXd m_q_sum;
  Eigen::VectorXd m_delta_q_target;
  Eigen::VectorXd m_q_target0;
public:
  LowerPositionVariation();
  LowerPositionVariation(const virtualModelPtr& p_mpc_model);
  LowerPositionVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max);
  void init(const virtualModelPtr& p_mpc_model);
  void setMechanicalJointLimits(const Eigen::VectorXd& q_min);
  void setLimits(std::vector<double>& delta_q_max);
  void setLimits(const Eigen::VectorXd& delta_q_max);
  void setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<LowerPositionVariation> LowerPositionVariationPtr;

/*****************************
 *
 *  UpperInvarianceVariation
 *
 *****************************/

class UpperInvarianceVariation : public MotionConstraint
{
protected:
  Eigen::VectorXd m_delta_q_max;
  Eigen::VectorXd m_q_max;
  Eigen::VectorXd m_q_sum;
  Eigen::VectorXd m_delta_q_target;
  Eigen::VectorXd m_q_target0;
  Eigen::MatrixXd m_Kinv;
public:
  UpperInvarianceVariation();
  UpperInvarianceVariation(const virtualModelPtr& p_mpc_model);
  UpperInvarianceVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max, std::vector<double>& dqmax, std::vector<double>& ddqmin);
  void init(const virtualModelPtr& p_mpc_model);
  void setMechanicalJointLimits(const Eigen::VectorXd& q_max);
  void setLimits(std::vector<double>& delta_q_max, std::vector<double>& dqmax, std::vector<double>& ddqmin);
  void setLimits(const Eigen::VectorXd& delta_q_max);
  void setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<UpperInvarianceVariation> UpperInvarianceVariationPtr;

/*****************************
 *
 *  LowerInvarianceVariation
 *
 *****************************/

class LowerInvarianceVariation : public MotionConstraint
{
protected:
  Eigen::VectorXd m_delta_q_max;
  Eigen::VectorXd m_q_min;
  Eigen::VectorXd m_q_sum;
  Eigen::VectorXd m_delta_q_target;
  Eigen::VectorXd m_q_target0;
  Eigen::MatrixXd m_Kinv;
public:
  LowerInvarianceVariation();
  LowerInvarianceVariation(const virtualModelPtr& p_mpc_model);
  LowerInvarianceVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max, std::vector<double>& dqmin, std::vector<double>& ddqmax);
  void init(const virtualModelPtr& p_mpc_model);
  void setMechanicalJointLimits(const Eigen::VectorXd& q_min);
  void setLimits(std::vector<double>& delta_q_max, std::vector<double>& dqmin, std::vector<double>& ddqmax);
  void setLimits(const Eigen::VectorXd& delta_q_max);
  void setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target);
  void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<LowerInvarianceVariation> LowerInvarianceVariationPtr;

/****************************
 *
 *  ScalingLimits
 *
 ****************************/

class ScalingLimits : public MotionConstraint
{
public:
  ScalingLimits();
  ScalingLimits(const virtualModelPtr& p_mpc_model, const double& max_scaling);
  ScalingLimits(const virtualModelPtr& p_mpc_model, const double& min_scaling, const double& max_scaling);
  void init(const virtualModelPtr& p_mpc_model);
  void setLimits(const double& max_scaling);
  void setLimits(const double& min_scaling,const double& max_scaling);
};
typedef std::shared_ptr<ScalingLimits> ScalingLimitsPtr;

}
}

#endif
