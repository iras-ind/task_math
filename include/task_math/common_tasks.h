#ifndef __common_tasks__
#define __common_tasks__

#include "Eigen/Dense"
#include <ros/console.h>
#include <eigen_matrix_utils/eiquadprog.hpp>
#include <rosdyn_core/primitives.h>
#include <rosdyn_core/frame_distance.h>
#include <task_math/task_math.h>

namespace taskQP
{

namespace math
{

/***********************
 *
 *  MinimizeAcceleration
 *
 ***********************/

class MinimizeAcceleration : public MotionConstraint
{
public:
  MinimizeAcceleration();
  MinimizeAcceleration(const virtualModelPtr& p_mpc_model);
  void init(const virtualModelPtr& p_mpc_model);
};
typedef std::shared_ptr<MinimizeAcceleration> MinimizeAccelerationPtr;

/***********************
 *
 *  MinimizeVelocity
 *
 ***********************/

class MinimizeVelocity : public MotionConstraint
{
public:
  MinimizeVelocity();
  MinimizeVelocity(const virtualModelPtr& p_mpc_model);
  void init(const virtualModelPtr& p_mpc_model);
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<MinimizeVelocity> MinimizeVelocityPtr;

/***********************
 *
 *  JointPositionTask
 *
 ***********************/

class JointPositionTask : public MotionConstraint
{
public:
  JointPositionTask();
  JointPositionTask(const virtualModelPtr& p_mpc_model);
  void init(const virtualModelPtr& p_mpc_model);
  void setTargetPosition(const Eigen::VectorXd& q_des);
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
  virtual void update(const Eigen::VectorXd& q_des, const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<JointPositionTask> JointPositionTaskPtr;

/***********************
 *
 *  JointVelocityTask
 *
 ***********************/

class JointVelocityTask : public MotionConstraint
{
protected:
  bool m_scaling_active;
  bool m_need_update;
  Eigen::VectorXd m_dq_target;
  Eigen::VectorXd m_next_q;
  bool m_is_clik_enabled;
  double m_lambda_clik;
  void reinit();

public:
  JointVelocityTask();
  JointVelocityTask(const virtualModelPtr& p_mpc_model);
  void init(const virtualModelPtr& p_mpc_model);
  void enableClik( const bool& enable_clik );
  bool isClikEnabled();
  void setWeightClik( const double& lambda_clik );
  double getWeightClik();
  void activateScaling(const bool& scaling);
  void setWeightScaling(const double& weight);
  void setTargetVelocity(const Eigen::VectorXd& dq_des);
  void setTargetTrajectory(const Eigen::VectorXd& dq_des, const Eigen::VectorXd& next_targetQ);
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
  virtual void update(const Eigen::VectorXd& dq_des, const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<JointVelocityTask> JointVelocityTaskPtr;

/***********************
 *
 *  ChainTask
 *
 ***********************/

class ChainTask : public MotionConstraint
{
protected:
  rosdyn::ChainPtr m_chain;
  unsigned int m_task_size;
  unsigned int m_nax_chain;
  bool m_need_update;

public:
  ChainTask();
  void setDynamicsChain(const rosdyn::ChainPtr&  chain);
  rosdyn::ChainPtr getDynamicsChain();
  unsigned int getTaskSize();
  Eigen::Affine3d getTransformation();
  Eigen::Vector6d getTwist();
};
typedef std::shared_ptr<ChainTask> ChainTaskPtr;


/***********************
 *
 *  CartesianTask
 *
 ***********************/

class CartesianTask : public ChainTask
{
protected:
  Eigen::MatrixXd m_select_task_axis_matrix;
  bool m_is_clik_enabled;
  double m_lambda_clik;
  bool m_scaling_active;
  Eigen::VectorXd m_dx_target;
  Eigen::Affine3d m_next_x_target;
  void reinit();
  virtual void computeActualMatrices(const Eigen::VectorXd& targetDx, const Eigen::VectorXd& q);

public:
  CartesianTask();
  CartesianTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task);
  CartesianTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task, const std::vector<int>& selection_vector);
  void init(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task, const std::vector<int>& selection_vector);
  void enableClik( const bool& enable_clik );
  bool isClikEnabled();
  void setWeightClik( const double& lambda_clik );
  double getWeightClik();
  void activateScaling(const bool& scaling);
  void setWeightScaling(const double& weight);
  void computeTaskSelectionMatrix(const std::vector<int>& selection_vector);
  Eigen::VectorXd computeTaskError(const Eigen::Affine3d& next_targetX, const Eigen::VectorXd& joint_position);
  void setTargetTrajectory(const Eigen::VectorXd& dx_des, const Eigen::Affine3d& next_targetX);
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
  virtual void update(const Eigen::VectorXd& targetDx, const Eigen::Affine3d& next_targetX, const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<CartesianTask> CartesianTaskPtr;

/***********************
 *
 *  BasicClearanceTask
 *
 ***********************/

class BasicClearanceTask : public ChainTask
{
protected:
  double m_clearance_velocity_gain;
  Eigen::Affine3d m_obstacle_pose;
  void reinit();
  void computeActualMatrices( const Eigen::Affine3d& obstacle_pose, const Eigen::VectorXd& q);

public:
  BasicClearanceTask();
  BasicClearanceTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task);
  void init(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task);
  void setClearanceVelocityGain( const double& velocity_gain );
  double getClearanceVelocityGain();
  void setObstaclePose(const Eigen::Affine3d& obstacle_pose);
  virtual void update(const Eigen::Affine3d& obstacle_pose, const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
};
typedef std::shared_ptr<BasicClearanceTask> BasicClearanceTaskPtr;

}
}

#endif
