#ifndef __task_math__
#define __task_math__

#include "Eigen/Dense"
#include <ros/console.h>
#include <eigen_matrix_utils/eiquadprog.hpp>
#include <rosdyn_core/primitives.h>
#include <rosdyn_core/frame_distance.h>

namespace taskQP
{

namespace math
{

class virtualModel // usarlo con puntatori in modo da averne solo una copia?
{
protected:
  bool   m_need_init;
  double m_st;
  Eigen::VectorXd m_state;
  bool m_state_has_changed;
  Eigen::VectorXd m_last_sol;
  Eigen::VectorXd m_position_prediction;
  Eigen::VectorXd m_velocity_prediction;
  unsigned int m_nax;
  unsigned int m_np;
  Eigen::VectorXd m_prediction_times;
  Eigen::MatrixXd m_position_free_resp;
  Eigen::MatrixXd m_position_forced_resp;
  Eigen::MatrixXd m_velocity_free_resp;
  Eigen::MatrixXd m_velocity_forced_resp;
  double m_predictive_horizon;
  bool m_use_input_blocking;
  Eigen::MatrixXd freeResponse(const double& t, const unsigned int& nax);
  Eigen::MatrixXd forcedResponse(const double& t, const unsigned int& nax);
  bool computeEvolutionMatrix( const Eigen::Ref<Eigen::VectorXd>& prediction_time,
                               const Eigen::Ref<Eigen::VectorXd>& control_intervals,
                               const unsigned int& nax,
                               Eigen::MatrixXd& free_response,
                               Eigen::MatrixXd& forced_response
                              );

  void splitResponses( const Eigen::MatrixXd& free_response,
                       Eigen::MatrixXd& velocity_free_response,
                       Eigen::MatrixXd& position_free_response,
                       const Eigen::MatrixXd& forced_response,
                       Eigen::MatrixXd& velocity_forced_response,
                       Eigen::MatrixXd& position_forced_response,
                       const unsigned int& nax);
  bool quadraticControlIntervals( const double& control_horizon_time, const unsigned int& n_control, const double& sampling_period, Eigen::VectorXd& control_intervals, Eigen::VectorXd& prediction_time );
  bool constantControlIntervals ( const double& control_horizon_time, const unsigned int& n_control, const double& sampling_period, Eigen::VectorXd& control_intervals, Eigen::VectorXd& prediction_time );

public:
  virtualModel();
  bool init(const unsigned int& nax, const unsigned int& n_control, const double& sampling_period );
  bool init();
  void setPredictiveHorizon( const double& predictive_horizon );
  bool useInputBlocking(bool& use_input_blocking);
  Eigen::MatrixXd getPositionFreeResp();
  Eigen::MatrixXd getPositionForcedResp();
  Eigen::MatrixXd getVelocityFreeResp();
  Eigen::MatrixXd getVelocityForcedResp();
  void updatePredictions(const Eigen::VectorXd& sol);
  Eigen::VectorXd getPositionPrediction();
  Eigen::VectorXd getVelocityPrediction();
  void set_n_axis(const unsigned int& n_ax);
  void set_np(const unsigned int& np);
  unsigned int get_n_axis();
  unsigned int get_np();
  Eigen::VectorXd getPredictionTimes();
  void setSamplingPeriod(const double& st);
  double getSamplingPeriod();
  bool needInitialization();
  void setInitialState(const Eigen::VectorXd& x0);
  void updateState( const Eigen::VectorXd& next_acc );
  Eigen::VectorXd getState();
};
typedef std::shared_ptr<virtualModel> virtualModelPtr;

class BaseConstraint
{
protected:
  Eigen::MatrixXd m_A;
  Eigen::VectorXd m_b;
  Eigen::MatrixXd m_W;
  bool m_is_inequality;
  bool m_is_a_maximization_problem;
  std::string m_type; // fare enum??

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  BaseConstraint();
  virtual void update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq);
  Eigen::MatrixXd A();
  Eigen::VectorXd b();
  Eigen::MatrixXd W();
  unsigned int rows();
  unsigned int cols();
  void set_as_inequality(const bool& is_inequality);
  void set_as_maximization_problem(const bool& is_max_problem);
  bool is_an_inequality();
  bool is_it_a_maximization_problem();
  void setWeightingMatrix(const Eigen::MatrixXd& W);
  const std::string& getType(){return m_type;}
};
typedef std::shared_ptr<BaseConstraint> BaseConstraintPtr;

class MotionConstraint : public BaseConstraint
{
protected:
  unsigned int m_nax;
  unsigned int m_np;
  double m_st;
  Eigen::MatrixXd m_A_pos;
  Eigen::MatrixXd m_A_vel;
  Eigen::MatrixXd m_A_acc;
  Eigen::MatrixXd m_A_scaling;
  bool m_is_A_pos_initialized;
  bool m_is_A_vel_initialized;
  bool m_is_A_acc_initialized;
  bool m_is_A_scaling_initialized;
  Eigen::VectorXd m_b_0;
  Eigen::MatrixXd m_do_scaling; // applying the scaling to trajectory
  double m_sref;
  double m_weight_scaling;
  virtualModelPtr m_mpc_matrices;
  virtual void computeTaskMatrices(const Eigen::VectorXd& q, const Eigen::VectorXd& dq, Eigen::MatrixXd& A, Eigen::VectorXd& b);
  virtual void reinit();

public:
  MotionConstraint();
  void initMPC(const virtualModelPtr& mpc_matrices);
  void set_n_axis(const unsigned int& n_ax);
  void set_np(const unsigned int& np);
  double getSamplingPeriod();
  void setPredictiveHorizon( const double& predictive_horizon );
  void setTargetScaling(const double& s_ref);
  virtual void setWeightScaling(const double& weight);
};

class TaskStack
{
protected:
  unsigned int m_nax;
  unsigned int m_np;
  std::vector<BaseConstraintPtr> m_stack;
  std::vector<double> m_weights;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  TaskStack();
  void set_n_axis(const unsigned int& n_ax);
  void set_np(const unsigned int& np);
  void taskPushBack( const BaseConstraintPtr& new_task);
  void taskPushBack( const BaseConstraintPtr& new_task, const double& lambda);
  void taskOverwrite( const BaseConstraintPtr& new_task, const unsigned int& index);
  void taskOverwrite( const BaseConstraintPtr& new_task, const unsigned int& index, const double& lambda);
  void deleteTask( const unsigned int& index);
  void swapTasks( const unsigned int& index1, const unsigned int& index2);
  BaseConstraintPtr getTask(const unsigned int& index);
  double taskWeight(const unsigned int& index);
  unsigned int stackSize();
  void setWeight(const double& lambda, const unsigned int& index);
  void setWeightSmooth(const double& lambda, const unsigned int& index);
};

class LimitsArray // gestire differenze tra m_nax sot e singolo task
{
protected:
  unsigned int m_nax;
  unsigned int m_np;
  std::vector<BaseConstraintPtr> m_array;
  Eigen::MatrixXd m_CM;
  Eigen::VectorXd m_c0;
  std::vector<unsigned int> m_indices_begin;
  std::vector<unsigned int> m_task_sizes;
  void update();

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  LimitsArray();
  void set_n_axis(const unsigned int& n_ax);
  void set_np(const unsigned int& np);
  void addConstraint(const BaseConstraintPtr& new_constraint);
  void deleteConstraint(const unsigned int& index);
  BaseConstraintPtr getConstraint(const unsigned int& index);
  unsigned int arraySize();
  Eigen::MatrixXd matrix();
  Eigen::VectorXd vector();
};

double computeWeightedSolution(TaskStack& sot, const Eigen::MatrixXd& CE, const Eigen::VectorXd& ce0, const Eigen::MatrixXd& CI, const Eigen::VectorXd& ci0 , Eigen::VectorXd &sol);
double computeHQPSolution(TaskStack& sot, const Eigen::MatrixXd& CE, const Eigen::VectorXd& ce0, const Eigen::MatrixXd& CI, const Eigen::VectorXd& ci0 , Eigen::VectorXd &sol);

}

}

#endif
