#include <task_math/task_math.h>
#include <task_math/common_tasks.h>
#include <task_math/common_limits.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "test_task_math");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  double st=0.008;
  unsigned int nax=7;
  unsigned int nc=5;

  urdf::Model model;
  model.initParam("robot_description");
  Eigen::Vector3d grav;
  grav << 0, 0, -9.806;
  std::string base_frame = "world";
  std::string tool_frame = "ur5_ee_link";
  std::string elbow_frame = "ur5_forearm_link";

  rosdyn::ChainPtr chain_ee = rosdyn::createChain(model,base_frame,tool_frame,grav);
  rosdyn::ChainPtr chain_elbow = rosdyn::createChain(model,base_frame,elbow_frame,grav);


  taskQP::math::virtualModelPtr mpc_mat_cart(new taskQP::math::virtualModel());
  mpc_mat_cart->setPredictiveHorizon(0.5);
  mpc_mat_cart->init(nax,nc,st);

  // test constructors
  taskQP::math::MinimizeAccelerationPtr min_acc(new taskQP::math::MinimizeAcceleration(mpc_mat_cart));
  taskQP::math::MinimizeVelocityPtr min_vel(new taskQP::math::MinimizeVelocity(mpc_mat_cart));
  taskQP::math::JointPositionTaskPtr pos_task(new taskQP::math::JointPositionTask(mpc_mat_cart));
  taskQP::math::JointVelocityTaskPtr vel_task(new taskQP::math::JointVelocityTask(mpc_mat_cart));
  taskQP::math::CartesianTaskPtr cartesian_task(new taskQP::math::CartesianTask(mpc_mat_cart,chain_ee));
  taskQP::math::CartesianTaskPtr cartesian_task2(new taskQP::math::CartesianTask());
  std::vector<int> selection_vector{1, 1, 1, 0, 0, 0};
  taskQP::math::CartesianTaskPtr cartesian_task3(new taskQP::math::CartesianTask(mpc_mat_cart,chain_ee,selection_vector));
  taskQP::math::BasicClearanceTaskPtr clearance_task(new taskQP::math::BasicClearanceTask(mpc_mat_cart,chain_elbow));

  min_acc->initMPC(mpc_mat_cart);
  cartesian_task->initMPC(mpc_mat_cart);
  clearance_task->initMPC(mpc_mat_cart);
  clearance_task->setClearanceVelocityGain(0.0002);

  selection_vector.at(3)=1;
  selection_vector.at(4)=1;
  selection_vector.at(5)=1;
  cartesian_task->computeTaskSelectionMatrix(selection_vector);

  bool scaling_enabled=true;
  cartesian_task->enableClik(true);
  cartesian_task->setWeightClik(0.0);
  cartesian_task->activateScaling(scaling_enabled);
  cartesian_task->setWeightScaling(0.00001);
  unsigned int nax_scaling=0;
  if (scaling_enabled)
    nax_scaling=1;
//  cartesian_task2.set_np(nc);
//  cartesian_task2.setDynamicsChain(chain_ee);
  ROS_INFO("qui");

  Eigen::VectorXd u(nax*nc);
  Eigen::VectorXd q(nax);
  Eigen::VectorXd dq(nax);
  Eigen::VectorXd targetDx(6*nc);
  Eigen::Affine3d targetX;
  Eigen::Affine3d obstacle;

  q.setOnes();
  dq.setZero();
  u.setZero();
  targetDx.setZero();
  targetX.linear().setIdentity();
  targetX.translation().setZero();
  obstacle.linear().setIdentity();
  obstacle.translation().setZero();

  for (unsigned int idx=0;idx<nc;idx++)
    targetDx.segment(6*idx,6) << 10,10,10,10,10,10;

  Eigen::VectorXd x0(2*nax);
  x0 << q, dq;
//  ROS_INFO("xo size = %u x %u", x0.rows(), x0.cols());
//  ROS_INFO("axis size = %u", mpc_mat_cart.get_n_axis());
  mpc_mat_cart->setInitialState(x0);

  Eigen::VectorXd pos_pred = mpc_mat_cart->getPositionPrediction();
  Eigen::VectorXd vel_pred = mpc_mat_cart->getVelocityPrediction();
  Eigen::VectorXd pos_pred_clear = mpc_mat_cart->getPositionPrediction();
  Eigen::VectorXd vel_pred_clear = mpc_mat_cart->getVelocityPrediction();
  ROS_INFO("qui");

  cartesian_task->update(targetDx,targetX,pos_pred,vel_pred);
  clearance_task->update(obstacle,pos_pred_clear,vel_pred_clear);

  taskQP::math::TaskStack sot;
  sot.taskPushBack(cartesian_task,1.0);
  sot.taskPushBack(clearance_task,0.0);
  sot.taskPushBack(min_acc,1e-12);

  Eigen::MatrixXd CE;
  Eigen::VectorXd ce0;
  Eigen::MatrixXd CI;
  Eigen::VectorXd ci0;
  Eigen::VectorXd sol((nax+nax_scaling)*nc);

  CE.resize(0,0);
  ce0.resize(0);
  CI.resize(0,0);
  ci0.resize(0);

  std::vector<double> up_limits{  10, 20, 30, 40, 50, 60, 70 };
  std::vector<double> lo_limits{ -10,-20,-30,-40,-50,-60,-70 };
  std::vector<double> qmax{  2, 2, 2, 2, 2, 2, 2 };
  std::vector<double> qmin{  -2, -2, -2, -2, -2, -2, -2 };

  taskQP::math::UpperAccelerationLimitsPtr up_acc_limits(new taskQP::math::UpperAccelerationLimits(mpc_mat_cart));
  taskQP::math::LowerAccelerationLimitsPtr lo_acc_limits(new taskQP::math::LowerAccelerationLimits(mpc_mat_cart));
  taskQP::math::UpperPositionLimitsPtr up_pos_limits(new taskQP::math::UpperPositionLimits(mpc_mat_cart));
  taskQP::math::LowerPositionLimitsPtr lo_pos_limits(new taskQP::math::LowerPositionLimits(mpc_mat_cart));
  taskQP::math::UpperVelocityLimitsPtr up_vel_limits(new taskQP::math::UpperVelocityLimits(mpc_mat_cart));
  taskQP::math::LowerVelocityLimitsPtr lo_vel_limits(new taskQP::math::LowerVelocityLimits(mpc_mat_cart));
  taskQP::math::ScalingLimitsPtr max_scaling(new taskQP::math::ScalingLimits(mpc_mat_cart,1.5));
  taskQP::math::UpperInvarianceConstraintPtr up_inv_limits(new taskQP::math::UpperInvarianceConstraint(mpc_mat_cart,qmax,qmax,up_limits));
  taskQP::math::LowerInvarianceConstraintPtr lo_inv_limits(new taskQP::math::LowerInvarianceConstraint(mpc_mat_cart,qmin,qmin,up_limits));

  up_acc_limits->setLimits(up_limits);
  lo_acc_limits->setLimits(lo_limits);
  up_pos_limits->setLimits(qmax);
  up_pos_limits->initMPC(mpc_mat_cart);
  up_pos_limits->update(q,dq);
  lo_pos_limits->setLimits(qmin);
  lo_pos_limits->initMPC(mpc_mat_cart);
  lo_pos_limits->update(q,dq);
  up_vel_limits->setLimits(qmax);
  up_vel_limits->initMPC(mpc_mat_cart);
  up_vel_limits->update(q,dq);
  lo_vel_limits->setLimits(qmin);
  lo_vel_limits->initMPC(mpc_mat_cart);
  lo_vel_limits->update(q,dq);
  up_inv_limits->initMPC(mpc_mat_cart);
  up_inv_limits->update(q,dq);
  lo_inv_limits->initMPC(mpc_mat_cart);
  lo_inv_limits->update(q,dq);



  taskQP::math::LimitsArray ineq_constraints;
  ineq_constraints.addConstraint(up_acc_limits);
  ineq_constraints.addConstraint(lo_acc_limits);
  ineq_constraints.addConstraint(up_pos_limits);
  ineq_constraints.addConstraint(lo_pos_limits);
  ineq_constraints.addConstraint(up_vel_limits);
  ineq_constraints.addConstraint(lo_vel_limits);
  ineq_constraints.addConstraint(up_inv_limits);
  ineq_constraints.addConstraint(lo_inv_limits);
  ineq_constraints.addConstraint(max_scaling);

//  ineq_constraints.deleteConstraint(1);
//  ROS_INFO_STREAM("CI=\n" << ineq_constraints.matrix());
//  ROS_INFO_STREAM("ci0=\n" << ineq_constraints.vector());

  taskQP::math::computeHQPSolution(sot,CE,ce0,ineq_constraints.matrix(),ineq_constraints.vector(),sol);
  ROS_INFO_STREAM("sol=" << sol.transpose());

  q+=dq.head(nax)*st+sol.head(nax)*st*st*0.5;
  dq+=sol.head(nax)*st;
  ROS_INFO_STREAM("new q = "<<  q.transpose());
  ROS_INFO_STREAM("new dq= "<< dq.transpose());
  ROS_INFO_STREAM("new dx= "<< chain_ee->getTwistTool(q,dq).transpose());
  ROS_INFO_STREAM("dx_elb=" << chain_elbow->getTwistTool(q.head(4),dq.head(4)).transpose());

  ros::shutdown();
  return 0;
}
