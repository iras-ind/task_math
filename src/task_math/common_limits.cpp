#include <task_math/task_math.h>
#include <task_math/common_limits.h>

namespace taskQP
{
namespace math
{


/****************************
 *
 *  UpperAccelerationLimits
 *
 ****************************/

UpperAccelerationLimits::UpperAccelerationLimits(){}

UpperAccelerationLimits::UpperAccelerationLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperAccelerationLimits::UpperAccelerationLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=-limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b.segment(idx*m_nax,m_nax)=limit_one_step;
}

void UpperAccelerationLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_A.setIdentity();
  m_b.resize(m_nax*m_np);
}

void UpperAccelerationLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=-limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

/****************************
 *
 *  LowerAccelerationLimits
 *
 ****************************/

LowerAccelerationLimits::LowerAccelerationLimits(){}

LowerAccelerationLimits::LowerAccelerationLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerAccelerationLimits::LowerAccelerationLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b.segment(idx*m_nax,m_nax)=limit_one_step;
}

void LowerAccelerationLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_A.setIdentity();
  m_A=-m_A;
  m_b.resize(m_nax*m_np);
}

void LowerAccelerationLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

/***********************
 *
 *  UpperVelocityLimits
 *
 ***********************/

UpperVelocityLimits::UpperVelocityLimits(){}

UpperVelocityLimits::UpperVelocityLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperVelocityLimits::UpperVelocityLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=-limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
}

void UpperVelocityLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void UpperVelocityLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=-limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void UpperVelocityLimits::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  LowerVelocityLimits
 *
 ***********************/

LowerVelocityLimits::LowerVelocityLimits(){}

LowerVelocityLimits::LowerVelocityLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerVelocityLimits::LowerVelocityLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
}

void LowerVelocityLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_A_vel=-m_A_vel;
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void LowerVelocityLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void LowerVelocityLimits::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  UpperPositionLimits
 *
 ***********************/

UpperPositionLimits::UpperPositionLimits(){}

UpperPositionLimits::UpperPositionLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperPositionLimits::UpperPositionLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=-limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
}

void UpperPositionLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_is_A_pos_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void UpperPositionLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=-limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void UpperPositionLimits::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  LowerPositionLimits
 *
 ***********************/

LowerPositionLimits::LowerPositionLimits(){}

LowerPositionLimits::LowerPositionLimits(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerPositionLimits::LowerPositionLimits(const virtualModelPtr& p_mpc_model, std::vector<double>& limits)
{
  init(p_mpc_model);
  if (m_nax!=limits.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd limit_one_step(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
    limit_one_step(idx)=limits.at(idx);
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
}

void LowerPositionLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_A_pos=-m_A_pos;
  m_is_A_pos_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void LowerPositionLimits::setLimits(std::vector<double>& limits)
{
  if (limits.size()==m_nax)
  {
    Eigen::VectorXd limit_one_step(m_nax);
    for (unsigned int idx=0;idx<m_nax;idx++)
      limit_one_step(idx)=limits.at(idx);
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b_0.segment(idx*m_nax,m_nax)=limit_one_step;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void LowerPositionLimits::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/*****************************
 *
 *  UpperInvarianceConstraint
 *
 *****************************/

UpperInvarianceConstraint::UpperInvarianceConstraint(){}

UpperInvarianceConstraint::UpperInvarianceConstraint(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperInvarianceConstraint::UpperInvarianceConstraint(const virtualModelPtr& p_mpc_model, std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmin)
{
  init(p_mpc_model);
  setLimits(qmax,dqmax,ddqmin);
}

void UpperInvarianceConstraint::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setZero();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_is_A_pos_initialized=true;
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void UpperInvarianceConstraint::setLimits(std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmin)
{
  if (m_nax!=dqmax.size() || m_nax!=ddqmin.size() || m_nax!=qmax.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd qmax_eigen(m_nax);
  Eigen::VectorXd dqmax_eigen(m_nax);
  Eigen::VectorXd ddqmin_eigen(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
  {
    qmax_eigen(idx)=qmax.at(idx);
    dqmax_eigen(idx)=std::abs(dqmax.at(idx));
    ddqmin_eigen(idx)=std::abs(ddqmin.at(idx));
  }
  Eigen::MatrixXd Kinv=0.99*(ddqmin_eigen.cwiseQuotient(dqmax_eigen)).asDiagonal();

  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_A_pos.block(m_nax*idx,m_nax*idx,m_nax,m_nax)=Kinv;
    m_b_0.segment(idx*m_nax,m_nax)=-Kinv*qmax_eigen;
  }
}

void UpperInvarianceConstraint::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/*****************************
 *
 *  LowerInvarianceConstraint
 *
 *****************************/

LowerInvarianceConstraint::LowerInvarianceConstraint(){}

LowerInvarianceConstraint::LowerInvarianceConstraint(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerInvarianceConstraint::LowerInvarianceConstraint(const virtualModelPtr& p_mpc_model, std::vector<double>& qmax, std::vector<double>& dqmax, std::vector<double>& ddqmax)
{
  init(p_mpc_model);
  setLimits(qmax,dqmax,ddqmax);
}

void LowerInvarianceConstraint::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setZero();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_A_vel=-m_A_vel;
  m_is_A_pos_initialized=true;
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void LowerInvarianceConstraint::setLimits(std::vector<double>& qmin, std::vector<double>& dqmin, std::vector<double>& ddqmax)
{
  if (m_nax!=dqmin.size() || m_nax!=ddqmax.size() || m_nax!=qmin.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  Eigen::VectorXd qmin_eigen(m_nax);
  Eigen::VectorXd dqmin_eigen(m_nax);
  Eigen::VectorXd ddqmax_eigen(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
  {
    qmin_eigen(idx)=qmin.at(idx);
    dqmin_eigen(idx)=std::abs(dqmin.at(idx));
    ddqmax_eigen(idx)=std::abs(ddqmax.at(idx));
  }
  Eigen::MatrixXd Kinv=0.99*(ddqmax_eigen.cwiseQuotient(dqmin_eigen)).asDiagonal();

  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_A_pos.block(m_nax*idx,m_nax*idx,m_nax,m_nax)=-Kinv;
    m_b_0.segment(idx*m_nax,m_nax)=Kinv*qmin_eigen;
  }
}

void LowerInvarianceConstraint::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  UpperPositionVariation
 *
 ***********************/

UpperPositionVariation::UpperPositionVariation(){}

UpperPositionVariation::UpperPositionVariation(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperPositionVariation::UpperPositionVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max)
{
  init(p_mpc_model);
  if (m_nax!=delta_q_max.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
      m_delta_q_max(inax+inp*m_nax)=delta_q_max.at(inax);
  }
}

void UpperPositionVariation::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_is_A_pos_initialized=true;
  m_A_scaling.resize(m_nax*m_np,m_np);
  m_A_scaling.setZero();
  m_is_A_scaling_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,(m_nax+1)*m_np);
  m_b.resize(m_nax*m_np);
  m_delta_q_max.resize(m_nax*m_np);
  m_delta_q_target.resize(m_nax*m_np);
  m_q_target0.resize(m_nax*m_np);
  m_q_max.resize(m_nax);
  m_q_max.setConstant(1.0e3);

  m_do_scaling.resize(m_nax*m_np,m_np);
  m_do_scaling.setZero();
  for (unsigned int ic=0;ic<m_np;ic++)
    m_do_scaling.block(ic*m_nax,ic,m_nax,1).setOnes();
}

void UpperPositionVariation::setMechanicalJointLimits(const Eigen::VectorXd& q_max)
{
  m_q_max=q_max;
}

void UpperPositionVariation::setLimits(std::vector<double>& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int inp=0;inp<m_np;inp++)
    {
      for (unsigned int inax=0;inax<m_nax;inax++)
        m_delta_q_max(inax+inp*m_nax)=delta_q_max.at(inax);
    }
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void UpperPositionVariation::setLimits(const Eigen::VectorXd& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int inp=0;inp<m_np;inp++)
      m_delta_q_max.segment(inp*m_nax,m_nax)=delta_q_max;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void UpperPositionVariation::setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target)
{
  m_q_target0.head(m_nax)=q_des_t0;
  m_q_target0.tail(m_nax*(m_np-1))=q_target.head(m_nax*(m_np-1));
  m_delta_q_target=q_target-m_q_target0;
}

void UpperPositionVariation::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  m_b_0=(-m_q_target0-m_delta_q_max);
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
    {
      if ( m_b_0(inp*m_nax+inax)-m_delta_q_target(inp*m_nax+inax)<=-m_q_max(inax) )
      {
        ROS_INFO("qui");

        m_delta_q_target(inp*m_nax+inax)=0.0;
        m_b_0(inp*m_nax+inax)=-m_q_max(inax);
      }
    }
  }
  m_A_scaling=-(m_delta_q_target.asDiagonal()*m_do_scaling);
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
//  ROS_INFO_STREAM_THROTTLE(2,"Apos=\n" << m_A_pos);
//  ROS_INFO_STREAM_THROTTLE(2,"As=\n" << m_A_scaling);
//  ROS_INFO_STREAM_THROTTLE(2,"b0=\n" << m_b_0.transpose());
//  ROS_INFO_STREAM_THROTTLE(2,"m_q_target0=\n" << m_q_target0.transpose());
//  ROS_INFO_STREAM_THROTTLE(2,"m_delta_q_target=\n" << m_delta_q_target.transpose());

}

/***********************
 *
 *  LowerPositionVariation
 *
 ***********************/

LowerPositionVariation::LowerPositionVariation(){}

LowerPositionVariation::LowerPositionVariation(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerPositionVariation::LowerPositionVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max)
{
  init(p_mpc_model);
  if (m_nax!=delta_q_max.size())
    ROS_FATAL("limits size is not equal to number of axis.");
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
      m_delta_q_max(inax+inp*m_nax)=delta_q_max.at(inax);
  }
}

void LowerPositionVariation::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_A_pos=-m_A_pos;
  m_is_A_pos_initialized=true;
  m_A_scaling.resize(m_nax*m_np,m_np);
  m_A_scaling.setZero();
  m_is_A_scaling_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,(m_nax+1)*m_np);
  m_b.resize(m_nax*m_np);
  m_delta_q_max.resize(m_nax*m_np);
  m_delta_q_target.resize(m_nax*m_np);
  m_q_target0.resize(m_nax*m_np);
  m_q_min.resize(m_nax);
  m_q_min.setConstant(-1.0e3);

  m_do_scaling.resize(m_nax*m_np,m_np);
  m_do_scaling.setZero();
  for (unsigned int ic=0;ic<m_np;ic++)
    m_do_scaling.block(ic*m_nax,ic,m_nax,1).setOnes();
}

void LowerPositionVariation::setMechanicalJointLimits(const Eigen::VectorXd& q_min)
{
  m_q_min=q_min;
}

void LowerPositionVariation::setLimits(std::vector<double>& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int inp=0;inp<m_np;inp++)
    {
      for (unsigned int inax=0;inax<m_nax;inax++)
        m_delta_q_max(inax+inp*m_nax)=delta_q_max.at(inax);
    }
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void LowerPositionVariation::setLimits(const Eigen::VectorXd& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int inp=0;inp<m_np;inp++)
      m_delta_q_max.segment(inp*m_nax,m_nax)=delta_q_max;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void LowerPositionVariation::setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target)
{
  m_q_target0.head(m_nax)=q_des_t0;
  m_q_target0.tail(m_nax*(m_np-1))=q_target.head(m_nax*(m_np-1));
  m_delta_q_target=q_target-m_q_target0;
}

void LowerPositionVariation::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  m_b_0=(m_q_target0-m_delta_q_max);
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
    {
      if ( m_b_0(inp*m_nax+inax)+m_delta_q_target(inp*m_nax+inax)<=m_q_min(inax) )
      {
        m_delta_q_target(inp*m_nax+inax)=0.0;
        m_b_0(inp*m_nax+inax)=m_q_min(inax);
      }
    }
  }
  m_A_scaling=m_delta_q_target.asDiagonal()*m_do_scaling;
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
//  ROS_ERROR_STREAM_THROTTLE(2,"Apos=\n" << m_A_pos);
//  ROS_ERROR_STREAM_THROTTLE(2,"As=\n" << m_A_scaling);
//  ROS_ERROR_STREAM_THROTTLE(2,"b0=\n" << m_b_0.transpose());
//  ROS_ERROR_STREAM_THROTTLE(2,"m_q_target0=\n" << m_q_target0.transpose());
//  ROS_ERROR_STREAM_THROTTLE(2,"m_delta_q_target=\n" << m_delta_q_target.transpose());
}

/*****************************
 *
 *  UpperInvarianceVariation
 *
 *****************************/

UpperInvarianceVariation::UpperInvarianceVariation(){}

UpperInvarianceVariation::UpperInvarianceVariation(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

UpperInvarianceVariation::UpperInvarianceVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max, std::vector<double>& dqmax, std::vector<double>& ddqmin)
{
  init(p_mpc_model);
  setLimits(delta_q_max,dqmax,ddqmin);
}

void UpperInvarianceVariation::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setZero();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_A_scaling.resize(m_nax*m_np,m_np);
  m_A_scaling.setZero();
  m_is_A_pos_initialized=true;
  m_is_A_vel_initialized=true;
  m_is_A_scaling_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,(m_nax+1)*m_np);
  m_b.resize(m_nax*m_np);
  m_delta_q_max.resize(m_nax*m_np);
  m_delta_q_target.resize(m_nax*m_np);
  m_q_target0.resize(m_nax*m_np);
  m_q_max.resize(m_nax);
  m_q_max.setConstant(1.0e3);
  m_q_sum.resize(m_nax*m_np);

  m_do_scaling.resize(m_nax*m_np,m_np);
  m_do_scaling.setZero();
  for (unsigned int ic=0;ic<m_np;ic++)
    m_do_scaling.block(ic*m_nax,ic,m_nax,1).setOnes();
}

void UpperInvarianceVariation::setMechanicalJointLimits(const Eigen::VectorXd& q_max)
{
  m_q_max=q_max;
}

void UpperInvarianceVariation::setLimits(std::vector<double>& delta_q_max, std::vector<double>& dqmax, std::vector<double>& ddqmin)
{
  if (m_nax!=dqmax.size() || m_nax!=ddqmin.size() || m_nax!=delta_q_max.size())
    ROS_FATAL("limits size is not equal to number of axis.");

  Eigen::VectorXd delta_q_max_eigen(m_nax);
  Eigen::VectorXd dqmax_eigen(m_nax);
  Eigen::VectorXd ddqmin_eigen(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
  {
    delta_q_max_eigen(idx)=delta_q_max.at(idx);
    dqmax_eigen(idx)=std::abs(dqmax.at(idx));
    ddqmin_eigen(idx)=std::abs(ddqmin.at(idx));
  }
  m_Kinv=0.98*(ddqmin_eigen.cwiseQuotient(dqmax_eigen)).asDiagonal();

  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_A_pos.block(m_nax*idx,m_nax*idx,m_nax,m_nax)=m_Kinv;
    m_delta_q_max.segment(idx*m_nax,m_nax)=delta_q_max_eigen;
//    m_b_0.segment(idx*m_nax,m_nax)=-m_Kinv*qmax_eigen;
  }
}

void UpperInvarianceVariation::setLimits(const Eigen::VectorXd& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int idx=0;idx<m_np;idx++)
      m_delta_q_max.segment(idx*m_nax,m_nax)=delta_q_max;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void UpperInvarianceVariation::setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target)
{
  m_q_target0.head(m_nax)=q_des_t0;
  m_q_target0.tail(m_nax*(m_np-1))=q_target.head(m_nax*(m_np-1));
  m_delta_q_target=0.99*(q_target-m_q_target0);
}

void UpperInvarianceVariation::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  m_q_sum=m_q_target0+m_delta_q_max;
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
    {
      if ( m_q_sum(inp*m_nax+inax)+m_delta_q_target(inp*m_nax+inax)>=m_q_max(inax) )
      {
        ROS_INFO("qui");
        m_delta_q_target(inp*m_nax+inax)=0.0;
        m_q_sum(inp*m_nax+inax)=m_q_max(inax);
      }
    }
    m_A_scaling.block(m_nax*inp,inp,m_nax,1)=-(m_Kinv*m_delta_q_target.segment(m_nax*inp,m_nax));
    m_b_0.segment(inp*m_nax,m_nax)=-m_Kinv*m_q_sum.segment(inp*m_nax,m_nax);
  }
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/*****************************
 *
 *  LowerInvarianceVariation
 *
 *****************************/

LowerInvarianceVariation::LowerInvarianceVariation(){}

LowerInvarianceVariation::LowerInvarianceVariation(const virtualModelPtr& p_mpc_model)
{
  init(p_mpc_model);
}

LowerInvarianceVariation::LowerInvarianceVariation(const virtualModelPtr& p_mpc_model, std::vector<double>& delta_q_max, std::vector<double>& dqmin, std::vector<double>& ddqmax)
{
  init(p_mpc_model);
  setLimits(delta_q_max,dqmin,ddqmax);

}

void LowerInvarianceVariation::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_is_inequality=true;
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setZero();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_A_vel=-m_A_vel;
  m_A_scaling.resize(m_nax*m_np,m_np);
  m_A_scaling.setZero();
  m_is_A_pos_initialized=true;
  m_is_A_vel_initialized=true;
  m_is_A_scaling_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,(m_nax+1)*m_np);
  m_b.resize(m_nax*m_np);
  m_delta_q_max.resize(m_nax*m_np);
  m_delta_q_target.resize(m_nax*m_np);
  m_q_target0.resize(m_nax*m_np);
  m_q_min.resize(m_nax);
  m_q_min.setConstant(-1.0e3);
  m_q_sum.resize(m_nax*m_np);

  m_do_scaling.resize(m_nax*m_np,m_np);
  m_do_scaling.setZero();
  for (unsigned int ic=0;ic<m_np;ic++)
    m_do_scaling.block(ic*m_nax,ic,m_nax,1).setOnes();
}

void LowerInvarianceVariation::setMechanicalJointLimits(const Eigen::VectorXd& q_min)
{
  m_q_min=q_min;
}

void LowerInvarianceVariation::setLimits(std::vector<double>& delta_q_max, std::vector<double>& dqmin, std::vector<double>& ddqmax)
{
  if (m_nax!=dqmin.size() || m_nax!=ddqmax.size() || m_nax!=delta_q_max.size())
    ROS_FATAL("limits size is not equal to number of axis.");

  Eigen::VectorXd delta_q_max_eigen(m_nax);
  Eigen::VectorXd dqmin_eigen(m_nax);
  Eigen::VectorXd ddqmax_eigen(m_nax);
  for (unsigned int idx=0;idx<m_nax;idx++)
  {
    delta_q_max_eigen(idx)=delta_q_max.at(idx);
    dqmin_eigen(idx)=std::abs(dqmin.at(idx));
    ddqmax_eigen(idx)=std::abs(ddqmax.at(idx));
  }
  m_Kinv=0.98*(ddqmax_eigen.cwiseQuotient(dqmin_eigen)).asDiagonal();

  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_A_pos.block(m_nax*idx,m_nax*idx,m_nax,m_nax)=-m_Kinv;
    m_delta_q_max.segment(idx*m_nax,m_nax)=delta_q_max_eigen;
//    m_b_0.segment(idx*m_nax,m_nax)=Kinv*qmin_eigen;
  }
}

void LowerInvarianceVariation::setLimits(const Eigen::VectorXd& delta_q_max)
{
  if (delta_q_max.size()==m_nax)
  {
    for (unsigned int idx=0;idx<m_np;idx++)
      m_delta_q_max.segment(idx*m_nax,m_nax)=delta_q_max;
  }
  else
    ROS_FATAL("limits size is not equal to number of axis.");
}

void LowerInvarianceVariation::setTargetTrajectory(const Eigen::VectorXd& q_des_t0, const Eigen::VectorXd& q_target)
{
  m_q_target0.head(m_nax)=q_des_t0;
  m_q_target0.tail(m_nax*(m_np-1))=q_target.head(m_nax*(m_np-1));
  m_delta_q_target=0.99*(q_target-m_q_target0);
}

void LowerInvarianceVariation::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  m_q_sum=m_q_target0-m_delta_q_max;
  for (unsigned int inp=0;inp<m_np;inp++)
  {
    for (unsigned int inax=0;inax<m_nax;inax++)
    {
      if ( m_q_sum(inp*m_nax+inax)+m_delta_q_target(inp*m_nax+inax)<=m_q_min(inax) )
      {
        m_delta_q_target(inp*m_nax+inax)=0.0;
        m_q_sum(inp*m_nax+inax)=m_q_min(inax);
      }
    }
    m_A_scaling.block(m_nax*inp,inp,m_nax,1)=m_Kinv*m_delta_q_target.segment(m_nax*inp,m_nax);
    m_b_0.segment(inp*m_nax,m_nax)=m_Kinv*m_q_sum.segment(inp*m_nax,m_nax);
  }
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/*****************************
 *
 *  ScalingLimits
 *
 *****************************/

ScalingLimits::ScalingLimits(){}

ScalingLimits::ScalingLimits(const virtualModelPtr& p_mpc_model, const double& max_scaling)
{
  init(p_mpc_model);
  setLimits(max_scaling);
}

ScalingLimits::ScalingLimits(const virtualModelPtr& p_mpc_model, const double& min_scaling, const double& max_scaling)
{
  init(p_mpc_model);
  setLimits(min_scaling,max_scaling);
}


void ScalingLimits::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_A.resize(2*m_np,(m_nax+1)*m_np);
  m_A.setZero();
  m_b.resize(2*m_np);
  m_b.setZero();
  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_A(idx,m_nax*m_np+idx)=1;
    m_A(m_np+idx,m_nax*m_np+idx)=-1.0;
    m_b(m_np+idx)=0.01; // min scaling
  }
}

void ScalingLimits::setLimits(const double& max_scaling)
{
  for (unsigned int idx=0;idx<m_np;idx++)
    m_b(idx)=-max_scaling;
}

void ScalingLimits::setLimits(const double& min_scaling,const double& max_scaling)
{
  for (unsigned int idx=0;idx<m_np;idx++)
  {
    m_b(m_np+idx)=min_scaling;
    m_b(idx)=-max_scaling;
  }
}

}
}
