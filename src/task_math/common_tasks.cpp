#include <task_math/task_math.h>
#include <task_math/common_tasks.h>

namespace taskQP
{
namespace math
{

/***********************
 *
 *  MinimizeAcceleration
 *
 ***********************/

MinimizeAcceleration::MinimizeAcceleration(){}

MinimizeAcceleration::MinimizeAcceleration(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_A.setIdentity();
  m_b.resize(m_nax*m_np);
  m_b.setZero();
}

void MinimizeAcceleration::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_A.setIdentity();
  m_b.resize(m_nax*m_np);
  m_b.setZero();
}

/***********************
 *
 *  MinimizeVelocity
 *
 ***********************/

MinimizeVelocity::MinimizeVelocity(){}

MinimizeVelocity::MinimizeVelocity(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void MinimizeVelocity::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void MinimizeVelocity::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  JointPositionTask
 *
 ***********************/

JointPositionTask::JointPositionTask(){}

JointPositionTask::JointPositionTask(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_is_A_pos_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void JointPositionTask::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_W.resize(m_nax*m_np,m_nax*m_np);
  m_W.setIdentity();
  m_A_pos.resize(m_nax*m_np,m_nax*m_np);
  m_A_pos.setIdentity();
  m_is_A_pos_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_A.resize(m_nax*m_np,m_nax*m_np);
  m_b.resize(m_nax*m_np);
}

void JointPositionTask::setTargetPosition(const Eigen::VectorXd& q_des)
{
  m_b_0=-q_des;
}

void JointPositionTask::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

void JointPositionTask::update(const Eigen::VectorXd& q_des, const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  m_b_0=-q_des;
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

/***********************
 *
 *  JointVelocityTask
 *
 ***********************/

JointVelocityTask::JointVelocityTask(){}

JointVelocityTask::JointVelocityTask(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  activateScaling(false);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  reinit();
}

void JointVelocityTask::init(const virtualModelPtr& p_mpc_model)
{
  MotionConstraint::initMPC(p_mpc_model);
  activateScaling(false);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  reinit();
}

void JointVelocityTask::activateScaling(const bool& scaling)
{
  if(scaling!=m_scaling_active)
  {
    m_scaling_active=scaling;
    m_is_A_scaling_initialized=scaling;
    if(m_scaling_active==true)
      ROS_ERROR("scaling enabled");
    reinit();
  }
  else
  {
    if(m_scaling_active==true)
      ROS_ERROR("scaling is already enabled");
    else
      ROS_ERROR("scaling is already disabled");
  }
}

void JointVelocityTask::reinit()
{
  m_A_vel.resize(m_nax*m_np,m_nax*m_np);
  m_A_vel.setIdentity();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_nax*m_np);
  m_b_0.setZero();
  m_do_scaling.resize(m_nax*m_np,m_np);
  m_do_scaling.setZero();
  for (unsigned int ic=0;ic<m_np;ic++)
    m_do_scaling.block(ic*m_nax,ic,m_nax,1).setOnes();
  unsigned int nax;
  if (m_scaling_active==true)
    nax=m_nax+1;
  else
    nax=m_nax;
  m_A.resize(nax*m_np,nax*m_np);
  m_A.setIdentity(nax*m_np,nax*m_np);
  m_b.resize(nax*m_np);
  m_b.setZero(nax*m_np);
  m_W.resize(nax*m_np,nax*m_np);
  m_W.setIdentity(nax*m_np,nax*m_np);
  setWeightScaling(m_weight_scaling);
  m_need_update=false;
}

void JointVelocityTask::enableClik( const bool& enable_clik )
{
  m_is_clik_enabled=enable_clik;
}
bool JointVelocityTask::isClikEnabled()
{
  return m_is_clik_enabled;
}
void JointVelocityTask::setWeightClik( const double& lambda_clik )
{
  m_lambda_clik=lambda_clik;
}
double JointVelocityTask::getWeightClik()
{
  return m_lambda_clik;
}

void JointVelocityTask::setWeightScaling(const double& weight)
{
  m_weight_scaling=weight;
  if (m_scaling_active==true)
  {
    for (unsigned int idx=1;idx<m_np;idx++)
      m_W(m_nax*m_np+idx,m_nax*m_np+idx)=m_weight_scaling;
  }
  else
    ROS_ERROR("Scaling is not active. Operation discarded.");
}

void JointVelocityTask::setTargetVelocity(const Eigen::VectorXd& dq_des)
{
  m_dq_target=dq_des;
}

void JointVelocityTask::setTargetTrajectory(const Eigen::VectorXd& dq_des, const Eigen::VectorXd& next_targetQ)
{
  m_dq_target=dq_des;
  m_next_q=next_targetQ;
}

void JointVelocityTask::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  if (m_is_clik_enabled)
    m_dq_target.segment(0,m_nax) += m_lambda_clik*(m_next_q-q.head(m_nax));
  if (m_scaling_active==false)
  {
    m_b_0=-m_dq_target;
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
  }
  else
  {
    Eigen::MatrixXd A_no_scaling(m_nax*m_np,(m_nax+1)*m_np);
    Eigen::VectorXd b_zero_vel(m_nax*m_np);
    m_A_scaling=-(m_dq_target.asDiagonal()*m_do_scaling);
    m_b_0.setZero();
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),A_no_scaling,b_zero_vel);
    m_A.block(0,0,m_nax*m_np,(m_nax+1)*m_np)=A_no_scaling;
//    m_A.block(0,m_nax*m_np,m_nax*m_np,m_np) = -(m_dq_target.asDiagonal()*m_do_scaling);
    m_b.head(m_nax*m_np)=b_zero_vel;
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b(m_nax*m_np+idx)=-m_sref;
  }
}

void JointVelocityTask::update(const Eigen::VectorXd& dq_des, const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{  
  if (m_scaling_active==false)
  {
    m_b_0=-dq_des;
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
  }
  else
  {
    Eigen::MatrixXd A_no_scaling(m_nax*m_np,m_nax*m_np);
    Eigen::VectorXd b_zero_vel(m_nax*m_np);
    m_b_0.setZero();
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),A_no_scaling,b_zero_vel);
    m_A.block(0,0,m_nax*m_np,m_nax*m_np)=A_no_scaling;
    m_A.block(0,m_nax*m_np,m_nax*m_np,m_np) = -(dq_des.asDiagonal()*m_do_scaling);
    m_b.head(m_nax*m_np)=b_zero_vel;
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b(m_nax*m_np+idx)=-m_sref;
  }
}

/***********************
 *
 *  ChainTask
 *
 ***********************/

ChainTask::ChainTask()
{
  ROS_DEBUG("New void chain task created.");
}

void ChainTask::setDynamicsChain(const rosdyn::ChainPtr&  chain)
{
  m_chain=chain;
  reinit();
  ROS_WARN("Dynamics chain updated.");
}

rosdyn::ChainPtr ChainTask::getDynamicsChain()
{
  return m_chain;
}

unsigned int ChainTask::getTaskSize()
{
  return m_task_size;
}

Eigen::Affine3d ChainTask::getTransformation()
{
  return m_chain->getTransformation(m_mpc_matrices->getState().head(m_nax_chain));
}

Eigen::Vector6d ChainTask::getTwist()
{
  return m_chain->getTwistTool(m_mpc_matrices->getState().head(m_nax_chain),m_mpc_matrices->getState().segment(m_nax,m_nax_chain));
}

/***********************
 *
 *  CartesianTask
 *
 ***********************/

CartesianTask::CartesianTask()
{
  m_nax=1;
  m_nax_chain=1;
  m_np=1;
  std::vector<int> selection_vector={1,1,1,1,1,1}; // default: all task axis activated
  computeTaskSelectionMatrix(selection_vector);
  m_scaling_active=false;
  activateScaling(m_scaling_active);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  ROS_DEBUG("New void cartesian task created.");
}

CartesianTask::CartesianTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_chain=chain_task;
  m_nax_chain=chain_task->getActiveJointsNumber();
  m_scaling_active=false;
  activateScaling(m_scaling_active);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  std::vector<int> selection_vector={1,1,1,1,1,1}; // default: all task axis activated
  computeTaskSelectionMatrix(selection_vector);
  ROS_DEBUG("New cartesian task created (all axis activated).");
  m_need_update=true;
  reinit();
}

CartesianTask::CartesianTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task, const std::vector<int>& selection_vector)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_chain=chain_task;
  m_nax_chain=chain_task->getActiveJointsNumber();
  ROS_INFO("m_nax_chain=%u",m_nax_chain);
  m_scaling_active=false;
  activateScaling(m_scaling_active);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  ROS_INFO("qui");
  computeTaskSelectionMatrix(selection_vector);
  ROS_INFO("New cartesian task created. Activated axis:");
  for (unsigned int idx=0;idx<6;idx++)
    printf("%d ",selection_vector.at(idx));
  printf("\n");
  m_need_update=true;
  reinit();
}

void CartesianTask::init(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task, const std::vector<int>& selection_vector)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_chain=chain_task;
  m_nax_chain=chain_task->getActiveJointsNumber();
  activateScaling(false);
  m_weight_scaling=1.0;
  m_sref=1.0;
  m_is_clik_enabled=false;
  m_lambda_clik=0.0;
  computeTaskSelectionMatrix(selection_vector);
  ROS_INFO("New cartesian task created. Activated axis:");
  for (unsigned int idx=0;idx<6;idx++)
    printf("%d ",selection_vector.at(idx));
  printf("\n");
  m_need_update=true;
  reinit();
}

void CartesianTask::enableClik( const bool& enable_clik )
{
  m_is_clik_enabled=enable_clik;
}
bool CartesianTask::isClikEnabled()
{
  return m_is_clik_enabled;
}
void CartesianTask::setWeightClik( const double& lambda_clik )
{
  m_lambda_clik=lambda_clik;
}
double CartesianTask::getWeightClik()
{
  return m_lambda_clik;
}

void CartesianTask::activateScaling(const bool& scaling)
{
  if(scaling!=m_scaling_active)
  {
    m_scaling_active=scaling;
    m_is_A_scaling_initialized=scaling;
    if(m_scaling_active==true)
      ROS_ERROR("scaling enabled");
    reinit();
  }
  else
  {
    if(m_scaling_active==true)
      ROS_ERROR("scaling is already enabled");
    else
      ROS_ERROR("scaling is already disabled");
  }
}

void CartesianTask::setWeightScaling(const double& weight)
{
  m_weight_scaling=weight;
  if (m_scaling_active==true)
  {
    for (unsigned int idx=0;idx<m_np;idx++)
      m_W(m_task_size*m_np+idx,m_task_size*m_np+idx)=m_weight_scaling;
  }
  else
    ROS_ERROR("Scaling is not active. Operation discarded.");
}

void CartesianTask::computeTaskSelectionMatrix(const std::vector<int>& selection_vector)
{
  std::vector<int> select_task_axis_vector=selection_vector;
  if (select_task_axis_vector.size()>6)
  {
    ROS_ERROR("length of 'select_task_axis_vector' greater than 6. Exceeding elements will be neglected.");
    select_task_axis_vector.resize(6);
  }
  else if (select_task_axis_vector.size()<6)
  {
    ROS_ERROR("length of 'select_task_axis_vector' smaller than 6. Missing elements will be set to zero.");
    unsigned int size_vec=select_task_axis_vector.size();
    select_task_axis_vector.resize(6);
    for (unsigned int idx=0;idx<6-size_vec;idx++)
      select_task_axis_vector.at(size_vec+idx)=0;
  }

  std::vector<int> indices;
  for (unsigned int idx=0;idx<select_task_axis_vector.size();idx++)
    if (select_task_axis_vector.at(idx)==1)
      indices.push_back(idx);
  m_select_task_axis_matrix.resize(indices.size(),select_task_axis_vector.size());

  for (unsigned int idx=0;idx<indices.size();idx++)
    m_select_task_axis_matrix.row(idx)=(Eigen::MatrixXd::Identity(6,6)).row(indices.at(idx));
  m_need_update=true;

  m_task_size=m_select_task_axis_matrix.rows();
  reinit();
}

Eigen::VectorXd CartesianTask::computeTaskError(const Eigen::Affine3d& next_targetX, const Eigen::VectorXd& joint_position)
{
  Eigen::Affine3d Tba=m_chain->getTransformation(joint_position.head(m_nax_chain)); // current ee pose
  Eigen::VectorXd task_error_in_b_full;
  rosdyn::getFrameDistance(next_targetX,Tba,task_error_in_b_full);
  Eigen::VectorXd task_error_in_b=m_select_task_axis_matrix*task_error_in_b_full;
  return task_error_in_b;
}

void CartesianTask::reinit()
{
  m_A_vel.resize(m_task_size*m_np,m_nax*m_np);
  m_A_vel.setZero();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_task_size*m_np);
  m_b_0.setZero();
  if (m_scaling_active==true)
  {
    m_do_scaling.resize(m_task_size*m_np,m_np);
    m_do_scaling.setZero();
    for (unsigned int ic=0;ic<m_np;ic++)
      m_do_scaling.block(ic*m_task_size,ic,m_task_size,1).setOnes();
  }
  unsigned int scaling_axis;
  if (m_scaling_active==true)
    scaling_axis=1;
  else
    scaling_axis=0;

  m_A.resize( (m_task_size+scaling_axis)*m_np, (m_nax+scaling_axis)*m_np );
  m_A.setZero();
  m_A.bottomRightCorner(m_np,m_np).setIdentity();
  m_b.resize( (m_task_size+scaling_axis)*m_np );
  m_b.setZero();
  m_W.resize( (m_task_size+scaling_axis)*m_np, (m_task_size+scaling_axis)*m_np);
  m_W.setIdentity();
  setWeightScaling(m_weight_scaling);
  m_need_update=false;
}

void CartesianTask::computeActualMatrices(const Eigen::VectorXd& targetDx, const Eigen::VectorXd& q)
{
  m_A_vel.setZero();
  for (unsigned int i_np=0; i_np<m_np; i_np++)
  {
    Eigen::MatrixXd jacobian_ee = m_select_task_axis_matrix*(m_chain->getJacobian(q.segment(i_np*m_nax,m_nax_chain)));
    m_A_vel.block(i_np*m_task_size,i_np*m_nax,m_task_size,m_nax_chain)=jacobian_ee;
    m_b_0.segment(i_np*m_task_size,m_task_size)=-m_select_task_axis_matrix*targetDx.segment(i_np*6,6);
  }
}

void CartesianTask::setTargetTrajectory(const Eigen::VectorXd& dx_des, const Eigen::Affine3d& next_targetX)
{
  m_dx_target=dx_des;
  m_next_x_target=next_targetX;
}

void CartesianTask::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  if (m_is_clik_enabled)
  {
    Eigen::VectorXd task_error_in_b=computeTaskError(m_next_x_target,q.head(m_nax_chain));
    m_dx_target.segment(0,m_task_size) += m_lambda_clik*task_error_in_b;
  }
  if (m_scaling_active==false)
  {
    computeActualMatrices(m_dx_target, q);
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
  }
  else
  {
    Eigen::MatrixXd A_no_scaling(m_task_size*m_np,(m_nax+1)*m_np);
    Eigen::VectorXd b_zero_vel(m_task_size*m_np);
    m_A_scaling=-(m_dx_target.asDiagonal()*m_do_scaling);
    m_b_0.setZero();
    computeActualMatrices(0.0*m_dx_target, q);
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),A_no_scaling,b_zero_vel);
    m_A.block(0,0,m_task_size*m_np,(m_nax+1)*m_np)=A_no_scaling;
//    m_A.block(0,m_nax*m_np,m_task_size*m_np,m_np) = -(m_dx_target.asDiagonal()*m_do_scaling);
    m_b.head(m_task_size*m_np)=b_zero_vel;
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b(m_task_size*m_np+idx)=-m_sref;
  }
}


void CartesianTask::update(const Eigen::VectorXd& targetDx, const Eigen::Affine3d& next_targetX, const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  Eigen::VectorXd targetDx_clik=targetDx;
  if (m_is_clik_enabled)
  {
    Eigen::VectorXd task_error_in_b=computeTaskError(next_targetX,q.head(m_nax_chain));
    for (unsigned int idx=0;idx<6;idx++)
      if (task_error_in_b(idx)>=0.01)
        task_error_in_b(idx)=0.01;
    targetDx_clik.segment(0,m_task_size) -= m_lambda_clik*task_error_in_b;
  }
  if (m_scaling_active==false)
  {
    computeActualMatrices(targetDx_clik, q);
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
  }
  else
  {
    Eigen::MatrixXd A_no_scaling(m_task_size*m_np,m_nax*m_np);
    Eigen::VectorXd b_zero_vel(m_task_size*m_np);
    m_b_0.setZero();
    computeActualMatrices(0.0*targetDx_clik, q);
    computeTaskMatrices(q.head(m_nax),dq.head(m_nax),A_no_scaling,b_zero_vel);
    m_A.block(0,0,m_task_size*m_np,m_nax*m_np)=A_no_scaling;
    m_A.block(0,m_nax*m_np,m_task_size*m_np,m_np) = -(targetDx_clik.asDiagonal()*m_do_scaling);
    m_b.head(m_task_size*m_np)=b_zero_vel;
    for (unsigned int idx=0;idx<m_np;idx++)
      m_b(m_task_size*m_np+idx)=-m_sref;
  }
}

/***********************
 *
 *  BasicClearanceTask
 *
 ***********************/

BasicClearanceTask::BasicClearanceTask()
{
  m_is_a_maximization_problem=false;
  m_clearance_velocity_gain=1;
  m_task_size=2;
  m_W.resize(m_task_size*m_np,m_task_size*m_np);
  m_W.setIdentity();
  m_need_update=true;
  ROS_DEBUG("New void clearance task created.");
}

BasicClearanceTask::BasicClearanceTask(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_chain=chain_task;
  m_is_a_maximization_problem=false;
  m_nax_chain=m_chain->getActiveJointsNumber();
  m_task_size=2;
  reinit();
  m_W.resize(m_task_size*m_np,m_task_size*m_np);
  m_W.setIdentity();
  m_clearance_velocity_gain=1;
  ROS_DEBUG("New clearance task created.");
}

void BasicClearanceTask::init(const virtualModelPtr& p_mpc_model, const rosdyn::ChainPtr& chain_task)
{
  MotionConstraint::initMPC(p_mpc_model);
  m_chain=chain_task;
  m_is_a_maximization_problem=false;
  m_nax_chain=m_chain->getActiveJointsNumber();
  m_task_size=2;
  reinit();
  m_W.resize(m_task_size*m_np,m_task_size*m_np);
  m_W.setIdentity();
  ROS_DEBUG("Clearance task initialized.");
}

void BasicClearanceTask::setClearanceVelocityGain( const double& velocity_gain )
{
  if (velocity_gain<0)
    ROS_FATAL("You tried to select a negative clearance velocity gain. A robot may not injure a human being, remember? Operation discarded.");
  else
    m_clearance_velocity_gain=velocity_gain;
}

double BasicClearanceTask::getClearanceVelocityGain(){ return m_clearance_velocity_gain; }

void BasicClearanceTask::reinit()
{
  m_A.resize(m_task_size*m_np,m_nax*m_np);
  m_b.resize(m_task_size*m_np);
  m_A_vel.resize(m_task_size*m_np,m_nax*m_np);
  m_A_vel.setZero();
  m_is_A_vel_initialized=true;
  m_b_0.resize(m_task_size*m_np);
  m_b_0.setZero();
  m_need_update=false;
}

void BasicClearanceTask::computeActualMatrices(const Eigen::Affine3d& obstacle_pose, const Eigen::VectorXd& q)
{  
  m_A_vel.setZero();
  for (unsigned int i_np=0; i_np<m_np; i_np++)
  {
    Eigen::MatrixXd jacobian_elbow = m_chain->getJacobian(q.segment(i_np*m_nax,m_nax_chain)).block(0,0,m_task_size,m_nax_chain);
    m_A_vel.block(i_np*m_task_size,i_np*m_nax,m_task_size,m_nax_chain)=jacobian_elbow;
    Eigen::Affine3d Tbe=m_chain->getTransformation(q.segment(i_np*m_nax,m_nax_chain)); // elbow pose
    m_b_0.segment(i_np*m_task_size,m_task_size)=( obstacle_pose.translation().head(2)-Tbe.translation().head(2) ).normalized()*m_clearance_velocity_gain;
  }
}

void BasicClearanceTask::setObstaclePose(const Eigen::Affine3d& obstacle_pose)
{
  m_obstacle_pose=obstacle_pose;
}

void BasicClearanceTask::update(const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeActualMatrices(m_obstacle_pose, q);
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

void BasicClearanceTask::update(const Eigen::Affine3d& obstacle_pose, const Eigen::VectorXd& q, const Eigen::VectorXd& dq)
{
  computeActualMatrices(obstacle_pose, q);
  computeTaskMatrices(q.head(m_nax),dq.head(m_nax),m_A,m_b);
}

}
}
